document.addEventListener('DOMContentLoaded', () => {
	/** @type HTMLInputElement */
	const input = document.getElementById('opacity');
	/** @type HTMLImageElement */
	const original = document.getElementById('original-image');
	const updateOpacity = () => (original.style.opacity = +input.value / 100);
	updateOpacity();

	input.addEventListener('input', () => {
		updateOpacity();
	});
});
