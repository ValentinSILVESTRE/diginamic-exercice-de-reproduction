# `TP - Intégration d'une maquette en HTML / CSS`

## `Instructions`

L'objectif de ce TP est de faire l'intégration d'une maquette en HTML CSS.

Aucun framework n'est autorisé. Si certains le souhaitent vous pouvez utiliser SASS.

Maquette Pizza : https://www.figma.com/file/nM10MLpeSlqiD0Ma4av9bM/Landing-Page-(Food-and-Beverage)-(Community)?node-id=0%3A1&t=Tlbfg8ewGm8V9J9F-1
